// BÀI 1: Tính tiền lương nhân viên
/**
 * INPUT:
 * workDay = số ngày làm việc;
 * dailyPay = 100000 (lương 1 ngày);
 *
 * TODO:
 * tính tiền lương
 * salary = workday * dailypay;
 *
 * OUTPUT:
 * salary;
 */
// START EX01
function tinhTienLuong() {
  var workDay = document.getElementById("num-work-day").value * 1;
  var dailyPay = 100000;
  document.getElementById("num-daily-pay").value = dailyPay;
  var salary = workDay * dailyPay;
  document.getElementById("num-salary").value = salary;
}
// END EX01

// BÀI 2: Tính giá trị trung bình
/**
 * INPUT
 * 5 số thực từ 1 đến 5: n1, n2, n3, n4, n5
 *
 * TODO
 * tính trung bình 5 số:
 * averageN = (n1 + n2 + n3 + n4 + n5) / 5;
 *
 * OUTPUT
 * averageN;
 */
// START EX02
var tinhTrungBinh = function () {
  var n1 = document.getElementById("num-ex02-n1").value * 1;
  var n2 = document.getElementById("num-ex02-n2").value * 1;
  var n3 = document.getElementById("num-ex02-n3").value * 1;
  var n4 = document.getElementById("num-ex02-n4").value * 1;
  var n5 = document.getElementById("num-ex02-n5").value * 1;
  var averageN = (n1 + n2 + n3 + n4 + n5) / 5;
  document.getElementById("gia-tri-trung-binh").innerText = averageN;
};
// END EX02

// BÀI 3: Quy đổi tiền
/**
 * INPUT
 * tyGia = 23500;
 * usdAmount = số tiền USD;
 *
 * TODO
 * vndAmount = usdAmount * tyGia;
 *
 * OUTPUT
 * vndAmount;
 */
// START EX03
var tyGia = 23500;
function exchangeToVND() {
  var usdAmount = document.getElementById("txt-so-tien-usd").value * 1;
  var vndAmount = usdAmount * tyGia;
  document.getElementById("txt-so-tien-vnd").innerHTML = `Số VND sẽ nhận được:
  <h4 class="h4 text-success m-0">${vndAmount}</h4>`;
}
// END EX03

// BÀI 4: Tính diện tích, chu vi hình chữ nhật
/**
 * INPUT
 * recLength = chiều dài;
 * recWidth = chiều rộng;
 *
 * TODO
 * recArea = recLength * recWidth; (diện tích)
 * recPerimeter = (recLength + recWidth) * 2; (chu vi)
 *
 * OUTPUT
 * recArea;
 * recPerimeter;
 */
// START EX04
function tinhChuNhat() {
  var recLength = document.getElementById("txt-ex04-length").value * 1;
  var recWidth = document.getElementById("txt-ex04-width").value * 1;
  var recArea = recLength * recWidth;
  var recPerimeter = (recLength + recWidth) * 2;
  document.getElementById("txt-dien-tich").innerText = recArea;
  document.getElementById("txt-chu-vi").innerText = recPerimeter;
}
// END EX04

// BÀI 5: Tính tổng 2 ký số
/**
 * INPUT
 * twoDigitNum: số có 2 chữ số;
 *
 * TODO
 * dozen = Math.floor(n / 10); (lấy chữ cố hàng chục)
 * unit = n % 10; (lấy chữ số hàng đơn vị)
 * sumNumber = dozen + unit; (tổng 2 ký số)
 *
 * OUTPUT
 * sumNumber;
 */
// START EX05
function tinhTongKySo() {
  var twoDigitNum = document.getElementById("txt-so-hai-chu").value * 1;
  var dozen = Math.floor(twoDigitNum / 10);
  var unit = twoDigitNum % 10;
  var sumNumber = dozen + unit;
  document.getElementById(
    "txt-tong-hai-ky-so"
  ).innerHTML = `Tổng nè: <span class="text-success">${sumNumber}</span>`;
}
// END EX05
